//
//  AppDelegate.swift
//  tabustasks
//
//  Created by ifau on 05/02/17.
//  Copyright © 2017 avium. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Moya

var auth_username = ""
var auth_password = ""

let orangeColor = UIColor(colorLiteralRed: 1, green: 75.0/255.0, blue: 0/255.0, alpha: 1)

let endpointClosure = { (target: TabusService) -> Endpoint<TabusService> in
    
    let credentialData = "\(auth_username):\(auth_password)".utf8Encoded
    let base64Credentials = credentialData.base64Encoded
    let defaultEndpoint = MoyaProvider.defaultEndpointMapping(for: target)
    
    return defaultEndpoint.adding(newHTTPHeaderFields: ["Authorization": "Basic \(base64Credentials)", "Accept-Language": "ru", "Content-Type":"application/json"])
}

func showError(title: String, message: String)
{
    DropdownAlert.showWithAnimation(animationType: .Spring(bounce: 15, speed: 12), title: title, message: message, backgroundColor: UIColor.red, textColor: UIColor.white, duration: 4)
}

func showAlert(title: String, message: String)
{
    DropdownAlert.showWithAnimation(animationType: .Spring(bounce: 15, speed: 12), title: title, message: message, backgroundColor: UIColor.gray, textColor: UIColor.white, duration: 4)
}

func showSuccess(title: String, message: String)
{
    DropdownAlert.showWithAnimation(animationType: .Spring(bounce: 15, speed: 12), title: title, message: message, backgroundColor: UIColor(red:0, green:0.8, blue:0, alpha: 1), textColor: UIColor.white, duration: 4)
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
        UINavigationBar.appearance().barTintColor = orangeColor
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        
        UINavigationBar.appearance().titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "HelveticaNeue-Light", size: 18)!]
        
        UITableView.appearance().separatorInset = UIEdgeInsetsMake(0, 16, 0, 16)
        
        DropdownAlert.defaultTitleFont = UIFont(name: "HelveticaNeue-Medium", size: 14)!
        DropdownAlert.defaultMessageFont = UIFont(name: "HelveticaNeue", size: 14)!
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension UIViewController
{
    func hideKeyboardWhenTappedAround()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
}

extension String
{
    var urlEscaped: String
    {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data
    {
        return self.data(using: .utf8)!
    }
}

extension Data
{
    var base64Encoded: String
    {
        return self.base64EncodedString(options: Data.Base64EncodingOptions.init(rawValue: 0))
    }
}
