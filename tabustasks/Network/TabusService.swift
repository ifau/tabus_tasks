//
//  TabusService.swift
//  tabustasks
//
//  Created by ifau on 05/02/17.
//  Copyright © 2017 avium. All rights reserved.
//

import Foundation
import Moya

enum TabusService
{
    case auth
    case tasksList(user: String, type: Int?, offset: Int, count: Int)
    case tasksInfo(user: String, number: Int)
    case tasksApprove(user: String, number: Int)
    case tasksCheck(user: String, number: Int)
}

// MARK: - TargetType Protocol Implementation

extension TabusService: TargetType
{
    var baseURL: URL { return URL(string: "http://my.tabus.ru/hs/api")! }
    
    var path: String
    {
        switch self
        {
            case .auth:
                return "/auth"
            case .tasksList(let user, let type, let offset, let count):
                if (type != nil)
                {
                    return "/tasks/list?user=\(user)&type=\(type)&offset=\(offset)&count=\(count)"
                }
                else
                {
                    return "/tasks/list?user=\(user)&offset=\(offset)&count=\(count)"
                }
            case .tasksInfo(let user, let number):
                return "/tasks/info?user=\(user)&number=\(number)"
            case .tasksApprove(let user, let number):
                return "/tasks/approve?user=\(user)&number=\(number)"
            case .tasksCheck(let user, let number):
                return "/tasks/check?user=\(user)&number=\(number)"
        }
    }
    
    var method: Moya.Method
    {
        switch self
        {
            case .auth, .tasksList, .tasksInfo, .tasksApprove, .tasksCheck:
                return .get
        }
    }
    
    var parameters: [String: Any]?
    {
        return nil;
    }
    
    var parameterEncoding: ParameterEncoding
    {
        return URLEncoding.default
    }
    
    var task: Task
    {
        return .request
    }
    
    var sampleData: Data
    {
        return "{}".utf8Encoded
    }
    
    var validate: Bool
    {
        return false
    }
}
