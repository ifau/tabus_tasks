//
//  TabusModels.swift
//  tabustasks
//
//  Created by ifau on 05/02/17.
//  Copyright © 2017 avium. All rights reserved.
//

import Foundation
import Mapper

// MARK: - Basic models

class TabusResponse: Mappable
{
    let api_ver: String?
    let state: Int?
    let message: String?
    
    required init(map: Mapper)
    {
        api_ver = map.optionalFrom("api_ver")
        state = map.optionalFrom("state")
        message = map.optionalFrom("message")
    }
}

class TabusTaskShort: Mappable
{
    let number: String?
    let type: Int?
    let name: String?
    let sum: Int?
    let laboriousness: Int?
    
    required init(map: Mapper)
    {
        number = map.optionalFrom("number")
        type = map.optionalFrom("type")
        name = map.optionalFrom("name")
        sum = map.optionalFrom("sum")
        laboriousness = map.optionalFrom("laboriousness")
    }
}

class TabusTaskFull: TabusTaskShort
{
    let description: String?
    let result: String?
    let statuses: [TabusTaskStatus]?
    
    required init(map: Mapper)
    {
        description = map.optionalFrom("description")
        result = map.optionalFrom("result")
        statuses = map.optionalFrom("statuses")
        super.init(map: map)
    }
}

class TabusTaskStatus: Mappable
{
    let date: Date?
    let performer: String?
    let name: String?
    
    required init(map: Mapper)
    {
        if let dateString: String = map.optionalFrom("date")
        {
            let dateformatter = DateFormatter()
            dateformatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss" // 2016-10-01T16:47:09
            date = dateformatter.date(from: dateString)
        }
        else
        {
            date = nil
        }
        
        performer = map.optionalFrom("performer")
        name = map.optionalFrom("name")
    }
}

// MARK: - Response Models

class AuthResponse: TabusResponse
{
    
}

class TasksListResponse: TabusResponse
{
    let items: [TabusTaskShort]?
    
    required init(map: Mapper)
    {
        items = map.optionalFrom("items")
        super.init(map: map)
    }
}

class TasksInfoResponse: TabusResponse
{
    let count: Int?
    let item: TabusTaskFull?
    
    required init(map: Mapper)
    {
        count = map.optionalFrom("count")
        item = map.optionalFrom("item")
        super.init(map: map)
    }
}

class TasksApproveResponse: TabusResponse
{
    
}

class TasksCheckResponse: TabusResponse
{
    
}
