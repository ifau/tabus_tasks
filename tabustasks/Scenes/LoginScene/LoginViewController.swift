//
//  LoginViewController.swift
//  tabustasks
//
//  Created by ifau on 05/02/17.
//  Copyright © 2017 avium. All rights reserved.
//

import UIKit
import Moya
import Moya_ModelMapper
import PasswordTextField

class LoginViewController: UIViewController
{
    @IBOutlet var loginTextField: UITextField!
    @IBOutlet var passwordTextField: PasswordTextField!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var loadingIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.setupUI()
    }
    
    var fistTimeAppear = true
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        if fistTimeAppear
        {
            fistTimeAppear = false
            
            if let username = UserDefaults.standard.value(forKey: "auth_username") as? String,
                let password = UserDefaults.standard.value(forKey: "auth_password") as? String
            {
                auth_username = username
                auth_password = password
                
                loginTextField.text = username
                passwordTextField.text = password
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    self.navigateToNextScene()
                })
            }
        }
    }
    
    @IBAction func loginButtonPressed(_ sender: Any)
    {
        if let username = loginTextField.text, let password = passwordTextField.text,
            username.characters.count > 0, password.characters.count > 0
        {
            auth_username = username
            auth_password = password
            
            tryAuthorize()
        }
        else
        {
            showAlert(title: "Действие невозможно", message: "Имя пользователя и пароль не заполнены")
        }
    }
    
    func tryAuthorize()
    {
        self.loadingIndicator.startAnimating()
        self.loadingIndicator.isHidden = false
        
        let provider = MoyaProvider<TabusService>(endpointClosure: endpointClosure)
        provider.request(.auth) { [unowned self] result in
            
            self.loadingIndicator.stopAnimating()
            self.loadingIndicator.isHidden = true
            
            switch result
            {
                case let .success(response):
                    do
                    {
                        if (response.statusCode == 200)
                        {
                            _ = try response.mapObject() as AuthResponse
                            self.saveCredentialData()
                            self.navigateToNextScene()
                        }
                        else if (response.statusCode >= 400 && response.statusCode < 410)
                        {
                            showError(title: "Ошибка авторизации", message: "Неверное имя пользователя или пароль")
                        }
                        else
                        {
                            showError(title: "Ошибка", message: "Ошибка выполнения на стороне сервера. Код ответа: \(response.statusCode)")
                        }
                    }
                    catch
                    {
                        showError(title: "Ошибка", message: "Неверный ответ со стороны сервера")
                    }
                case let .failure(error):
                    showError(title: "Ошибка соединения", message: "\(error.errorDescription ?? "Сервис недоступен или отсутствует подключение к интернету")")
            }
        }
    }
    
    func saveCredentialData()
    {
        UserDefaults.standard.set(auth_username, forKey: "auth_username")
        UserDefaults.standard.set(auth_password, forKey: "auth_password")
        UserDefaults.standard.synchronize()
    }
    
    func navigateToNextScene()
    {
        performSegue(withIdentifier: "LoginSegue", sender: nil)
    }
    
    let transitionMgr = WYInteractiveTransitions()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "LoginSegue"
        {
            let dvc = segue.destination
            transitionMgr.configureTransition(duration: 0.5, toView: dvc, panEnable: true, type: .zoom)
        }
    }
}

private extension LoginViewController
{
    func setupUI()
    {
        let titleView = UIImageView(frame: CGRect(x: 0, y: 0, width: 120, height: 40))
        titleView.contentMode = .scaleAspectFit
        titleView.image = UIImage(named: "tabus_logo")
        self.navigationItem.titleView = titleView
        loginButton.backgroundColor = orangeColor
        loginButton.layer.cornerRadius = 8
        loginButton.setTitleColor(UIColor.white, for: .normal)
        loginButton.titleLabel!.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        
        loginTextField.font = UIFont(name: "HelveticaNeue-Light", size: 14)
        loginTextField.backgroundColor = UIColor(colorLiteralRed: 0.97, green: 0.97, blue: 0.97, alpha: 1)
        loginTextField.tintColor = orangeColor
        
        passwordTextField.font = UIFont(name: "HelveticaNeue-Light", size: 14)
        passwordTextField.backgroundColor = UIColor(colorLiteralRed: 0.97, green: 0.97, blue: 0.97, alpha: 1)
        passwordTextField.tintColor = orangeColor
        passwordTextField.imageTintColor = orangeColor
        passwordTextField.showButtonWhile = .Always
        
        loadingIndicator.color = orangeColor
        loadingIndicator.isHidden = true
    }
}
