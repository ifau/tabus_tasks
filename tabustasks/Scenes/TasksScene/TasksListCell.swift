//
//  TasksListCell.swift
//  tabustasks
//
//  Created by ifau on 11/02/17.
//  Copyright © 2017 avium. All rights reserved.
//

import UIKit

class TasksListCell: UITableViewCell
{
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var sumLabel: UILabel!
    @IBOutlet var laboriousnessLabel: UILabel!
}
