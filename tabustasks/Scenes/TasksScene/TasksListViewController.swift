//
//  TasksListViewController.swift
//  tabustasks
//
//  Created by ifau on 11/02/17.
//  Copyright © 2017 avium. All rights reserved.
//

import UIKit
import Mapper
import BTNavigationDropdownMenu
import Moya

class TasksListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var tableView: UITableView!
    var sections = ["Все задачи", "Задачи на рассмотрении", "Задачи, ожидающие утверждения", "Задачи в работе", "Задачи, ожидающие проверки", "Закрытые задачи (Архив)", "Неотправленные задачи (Черновики)"]
    
    var allItems: [TabusTaskShort] = []
    var currentItems: [TabusTaskShort] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupUI()
        self.requestNext()
//        self.reloadDatasource()
    }
    
    // MARK: - Core functions
    
    var offset = 0
    var count = 200
    var type = 0
    {
        didSet
        {
            offset = 0
            requestNext()
        }
    }
    
    func reloadDatasource()
    {
        currentItems = allItems.filter{ type == 0 || ($0.type != nil && $0.type! == type) }
        tableView.reloadData()
    }
    
    func requestNext()
    {
        let _type: Int? = (type == 0) ? nil : type
        let provider = MoyaProvider<TabusService>(endpointClosure: endpointClosure)
        provider.request(.tasksList(user: auth_username, type: _type, offset: offset, count: count))
        { [unowned self] result in
            
            switch result
            {
            case let .success(response):
                do
                {
                    if (response.statusCode == 200)
                    {
                        let tasks = try response.mapObject() as TasksListResponse
                        self.allItems = (tasks.items != nil) ? tasks.items! : []
                        self.reloadDatasource()
                    }
                    else if (response.statusCode >= 400 && response.statusCode < 410)
                    {
                        showError(title: "Ошибка авторизации", message: "Необходимо повторить авторизацию")
                    }
                    else
                    {
                        showError(title: "Ошибка", message: "Ошибка выполнения на стороне сервера. Код ответа: \(response.statusCode)")
                    }
                }
                catch
                {
                    showError(title: "Ошибка", message: "Неверный ответ со стороны сервера")
                }
            case let .failure(error):
                showError(title: "Ошибка соединения", message: "\(error.errorDescription ?? "Сервис недоступен или отсутствует подключение к интернету")")
            }
        }
    }
    
    // MARK: - UITableView Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return currentItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TasksListCell", for: indexPath) as! TasksListCell
        let item = currentItems[indexPath.row]
        
        cell.numberLabel.text = (item.number != nil) ? "#\(item.number!)" : ""
        cell.nameLabel.text = item.name ?? ""
        cell.sumLabel.text = (item.sum != nil) ? "Стоимость: \(item.sum!) ₽" : ""
        cell.laboriousnessLabel.text = (item.laboriousness != nil) ? "Трудоёмкость: \(item.laboriousness!) ч." : ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        performSegue(withIdentifier: "TaskDetailsSegue", sender: indexPath)
    }
    
//    let transitionMgr = WYInteractiveTransitions()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "TaskDetailsSegue"
        {
            let indexPath = sender as! NSIndexPath
            let dvc = segue.destination as! TaskDetailsViewController
            dvc.tasknumber = currentItems[indexPath.row].number
//            transitionMgr.configureTransition(duration: 0.5, toView: dvc, panEnable: true, type: .up)
        }
    }
}

private extension TasksListViewController
{
    func setupUI()
    {
        initializeMenu()
        
        tableView.register(UINib(nibName: "TasksListCell", bundle: nil), forCellReuseIdentifier: "TasksListCell")
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
//        let dic = NSDictionary(dictionary: ["number":"005435", "type":0, "name":"Добавить поиск по названию", "sum":2500, "laboriousness":4])
//        let mapper = Mapper(JSON: dic)
//        let task = TabusTaskShort(map: mapper)
//        
//        let dic2 = NSDictionary(dictionary: ["number":"005612", "type":0, "name":"Разработать подсистему рассылки объектов подсистемы \"Разработка\"", "sum":3500, "laboriousness":10])
//        let mapper2 = Mapper(JSON: dic2)
//        let task2 = TabusTaskShort(map: mapper2)
//        
//        allItems = [task, task2]
    }
    
    func initializeMenu()
    {
        let menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: sections[0], items: sections as [AnyObject])
        menuView.navigationBarTitleFont = UIFont(name: "HelveticaNeue-Light", size: 18)!
        menuView.cellTextLabelFont = UIFont(name: "HelveticaNeue-Light", size: 14)!
        menuView.cellTextLabelColor = UIColor.white
        menuView.cellBackgroundColor = orangeColor
        menuView.cellSelectionColor = orangeColor
        menuView.checkMarkImage = nil
        menuView.menuTitleColor = UIColor.white
        menuView.animationDuration = 0.3
        menuView.cellSeparatorColor = UIColor.white
        menuView.maskBackgroundOpacity = 0.15
        
        self.navigationItem.titleView = menuView
        menuView.didSelectItemAtIndexHandler = {[weak self] (indexPath: Int) -> () in
            self?.type = indexPath
        }
    }
}
