//
//  TaskDetailsViewController.swift
//  tabustasks
//
//  Created by ifau on 19/02/17.
//  Copyright © 2017 avium. All rights reserved.
//

import UIKit
import Moya

class TaskDetailsViewController: UIViewController
{
    var tasknumber: String!
    
    @IBOutlet var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var spaceBeetwenLabels: [NSLayoutConstraint]!
    @IBOutlet var actionButton: UIButton!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var statusesLabel: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupUI()
        requestTaskInfo()
    }
    
    var taskDetails: TabusTaskFull?
    {
        didSet
        {
            if taskDetails != nil
            {
                updateLabels()
                updateButton()
                self.view.layoutIfNeeded()
                
                UIView.animate(withDuration: 0.5, animations:
                {
                    self.scrollView.alpha = 1.0
                },
                completion:
                { (Bool) in
                    
                    
                })
            }
        }
    }
}

extension TaskDetailsViewController
{
    func setupUI()
    {
        self.title = "#\(tasknumber!)"
        
        actionButton.isHidden = true
        actionButton.isEnabled = false
        actionButton.backgroundColor = orangeColor
        actionButton.layer.cornerRadius = 8
        actionButton.setTitleColor(UIColor.white, for: .normal)
        actionButton.titleLabel!.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        
        loadingIndicator.color = orangeColor
        loadingIndicator.isHidden = true
        scrollView.alpha = 0
    }
    
    func updateLabels()
    {
        if let taskInfo = taskDetails
        {
            let dTitle = NSMutableAttributedString(string: (taskInfo.name != nil) ? taskInfo.name! : "", attributes: [NSFontAttributeName:UIFont(name: "HelveticaNeue", size: 14)!, NSForegroundColorAttributeName: UIColor.black])
            let dDescr = NSMutableAttributedString(string: (taskInfo.description != nil) ? taskInfo.description! : "", attributes: [NSFontAttributeName:UIFont(name: "HelveticaNeue-Light", size: 12)!, NSForegroundColorAttributeName: UIColor.black])
            let dSum = NSMutableAttributedString(string: (taskInfo.sum != nil) ? "Стоимость: \(taskInfo.sum!) ₽" : "", attributes: [NSFontAttributeName:UIFont(name: "HelveticaNeue-Light", size: 12)!, NSForegroundColorAttributeName: UIColor.black])
            let dRes = NSMutableAttributedString(string: (taskInfo.result != nil) ? "Результат: \(taskInfo.result!)" : "", attributes: [NSFontAttributeName:UIFont(name: "HelveticaNeue-Light", size: 12)!, NSForegroundColorAttributeName: UIColor.black])
            
            let dSpace = NSMutableAttributedString(string: "\n\n", attributes: [NSFontAttributeName:UIFont(name: "HelveticaNeue-Light", size: 12)!, NSForegroundColorAttributeName: UIColor.black])
            
            let dFull = NSMutableAttributedString()
            if (dTitle.string.characters.count > 0)
            {
                dFull.append(dTitle)
            }
            if (dDescr.string.characters.count > 0)
            {
                dFull.append(dSpace)
                dFull.append(dDescr)
            }
            if (dSum.string.characters.count > 0)
            {
                dFull.append(dSpace)
                dFull.append(dSum)
            }
            if (dRes.string.characters.count > 0)
            {
                dFull.append(dSpace)
                dFull.append(dRes)
            }
            self.descriptionLabel.attributedText = dFull
            
            if let statuses = taskInfo.statuses
            {
                let sFull = NSMutableAttributedString()
                let df = DateFormatter()
                df.dateFormat = "dd.MM.yyyy hh:mm"
                
                for status in statuses
                {
                    if let date = status.date, let perf = status.performer, let name = status.name
                    {
                        let str = "\(df.string(from: date)) \(perf): \(name)"
                        sFull.append(NSMutableAttributedString(string: str, attributes: [NSFontAttributeName:UIFont(name: "HelveticaNeue", size: 10)!, NSForegroundColorAttributeName: UIColor.black]))
                        sFull.append(NSMutableAttributedString(string: "\n\n", attributes: [NSFontAttributeName:UIFont(name: "HelveticaNeue", size: 10)!, NSForegroundColorAttributeName: UIColor.black]))
                    }
                }
                self.statusesLabel.attributedText = sFull
            }
            else
            {
                self.statusesLabel.text = ""
            }
        }
    }
    
    func updateButton()
    {
        if let taskInfo = taskDetails, let type = taskInfo.type
        {
            if (type == 2 || type == 4)
            {
                actionButton.isHidden = false
                actionButton.isEnabled = true
                
                if (type == 2)
                {
                    actionButton.setTitle("Утвердить", for: .normal)
                    actionButton.addTarget(self, action: #selector(TaskDetailsViewController.approveButtonPressed), for: .touchUpInside)
                }
                
                if (type == 4)
                {
                    actionButton.setTitle("Проверить", for: .normal)
                    actionButton.addTarget(self, action: #selector(TaskDetailsViewController.checkButtonPressed), for: .touchUpInside)
                }
                
                for constraint in spaceBeetwenLabels
                {
                    constraint.constant = 16
                }
            }
        }
    }
}

extension TaskDetailsViewController
{
    func requestTaskInfo()
    {
        self.loadingIndicator.startAnimating()
        self.loadingIndicator.isHidden = false
        
        let num = Int(tasknumber)!
        let provider = MoyaProvider<TabusService>(endpointClosure: endpointClosure)
        provider.request(.tasksInfo(user: auth_username, number: num)) { [unowned self] result in
            
            self.loadingIndicator.stopAnimating()
            self.loadingIndicator.isHidden = true
            
            switch result
            {
            case let .success(response):
                do
                {
                    if (response.statusCode == 200)
                    {
                        let tasksInfoResponse = try response.mapObject() as TasksInfoResponse
                        self.taskDetails = tasksInfoResponse.item
                    }
                    else if (response.statusCode >= 400 && response.statusCode < 410)
                    {
                        showError(title: "Ошибка авторизации", message: "Необходимо повторить авторизацию")
                    }
                    else
                    {
                        showError(title: "Ошибка", message: "Ошибка выполнения на стороне сервера. Код ответа: \(response.statusCode)")
                    }
                }
                catch
                {
                    showError(title: "Ошибка", message: "Неверный ответ со стороны сервера")
                }
            case let .failure(error):
                showError(title: "Ошибка соединения", message: "\(error.errorDescription ?? "Сервис недоступен или отсутствует подключение к интернету")")
            }
        }
    }
    
    func approveButtonPressed()
    {
        actionButton.isEnabled = false
        
        let num = Int(tasknumber)!
        let provider = MoyaProvider<TabusService>(endpointClosure: endpointClosure)
        provider.request(.tasksApprove(user: auth_username, number: num)) { [unowned self] result in
            
            self.actionButton.isEnabled = true
            switch result
            {
            case let .success(response):
                do
                {
                    if (response.statusCode == 200)
                    {
                        let tasksApproveResponse = try response.mapObject() as TasksApproveResponse
                        showSuccess(title: "Запрос успешно выполнен", message: tasksApproveResponse.message ?? "")
                        
                    }
                    else if (response.statusCode >= 400 && response.statusCode < 410)
                    {
                        showError(title: "Ошибка авторизации", message: "Необходимо повторить авторизацию")
                    }
                    else
                    {
                        showError(title: "Ошибка", message: "Ошибка выполнения на стороне сервера. Код ответа: \(response.statusCode)")
                    }
                }
                catch
                {
                    showError(title: "Ошибка", message: "Неверный ответ со стороны сервера")
                }
            case let .failure(error):
                showError(title: "Ошибка соединения", message: "\(error.errorDescription ?? "Сервис недоступен или отсутствует подключение к интернету")")
            }
        }
    }
    
    func checkButtonPressed()
    {
        actionButton.isEnabled = false
        
        let num = Int(tasknumber)!
        let provider = MoyaProvider<TabusService>(endpointClosure: endpointClosure)
        provider.request(.tasksCheck(user: auth_username, number: num)) { [unowned self] result in
            
            self.actionButton.isEnabled = true
            switch result
            {
            case let .success(response):
                do
                {
                    if (response.statusCode == 200)
                    {
                        let tasksCheckResponse = try response.mapObject() as TasksCheckResponse
                        showSuccess(title: "Запрос успешно выполнен", message: tasksCheckResponse.message ?? "")
                        
                    }
                    else if (response.statusCode >= 400 && response.statusCode < 410)
                    {
                        showError(title: "Ошибка авторизации", message: "Необходимо повторить авторизацию")
                    }
                    else
                    {
                        showError(title: "Ошибка", message: "Ошибка выполнения на стороне сервера. Код ответа: \(response.statusCode)")
                    }
                }
                catch
                {
                    showError(title: "Ошибка", message: "Неверный ответ со стороны сервера")
                }
            case let .failure(error):
                showError(title: "Ошибка соединения", message: "\(error.errorDescription ?? "Сервис недоступен или отсутствует подключение к интернету")")
            }
        }
    }
}
